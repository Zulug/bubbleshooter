#if UNITY
using UnityEngine;
#endif

using System;

[Serializable]
public struct Point {
    public static readonly Point invalid = new Point(int.MaxValue, int.MaxValue);

    public int x;
    public int y;
    public int q => x;
    public int r => y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public override string ToString() {
        return $"[{x}, {y}]";
    }

    public override bool Equals(object other) {
        if (!(other is Point))
            return false;
        return this.Equals((Point) other);
    }

    public bool Equals(Point other) {
        return this.x.Equals(other.x) && this.y.Equals(other.y);
    }

    public static bool operator ==(Point lhs, Point rhs) {
        return lhs.x == rhs.x && lhs.y == rhs.y;
    }

    public static bool operator !=(Point lhs, Point rhs) {
        return !(lhs == rhs);
    }

    public static Point operator +(Point a, Point b) {
        return new Point(a.q + b.q, a.r + b.r);
    }

    public override int GetHashCode() {
        unchecked {
            return (q * 397) ^ r;
        }
    }
    
    //TODO
#if UNITY
    public bool IsNeighbour(Point other)
    {
        return (Mathf.Abs(x - other.x) == 1 && y == other.y) || (Mathf.Abs(y - other.y) == 1 && x == other.x);
    }
#endif
}