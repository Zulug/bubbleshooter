﻿using UnityEngine;

public class StateUI : MonoBehaviour
{
    protected bool _isActive = false;

    public virtual void Show()
    {
        _isActive = true;
    }
		
    public virtual void Hide()
    {
        _isActive = false;
    }
		
    public virtual void Pause()
    {
        _isActive = false;
    }
		
    public virtual void Resume()
    {
        _isActive = true;
    }

    protected T GetState<T>() where T : FSMState
    {
        return States.State<T>();
    }
}