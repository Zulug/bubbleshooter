﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public partial class FSM {
	class FSMTransition {
		public int transition;
		public FSMState from;
		public FSMState to;

		public FSMTransition(int transition, FSMState from, FSMState to) {
			this.transition = transition;
			this.from = from;
			this.to = to;
		}
	}

	public List<FSMState> states = new List<FSMState>();
	public string log;

	protected FSMState currentState = null;
	
	List<FSMTransition> _transitions = new List<FSMTransition>();
	List<FSMTransition> _popupTransitions = new List<FSMTransition>();
	FSMState _nextState;
	FSMState _nextStateToPush;

	Stack<FSMState> _queue = new Stack<FSMState>(); // pushed state
	bool _isSwitching;

	public void Start() {
		Switch(states[0]);
	}

//    public bool IsSwitchingState
//    {
//        get { return _nextState != null; }
//    }

	public void SpawnEvent(int eventName) {
		foreach (FSMTransition t in _transitions) {
			if (t.transition == eventName) {
				if (t.from == null || t.from == currentState) {
					Switch(t.to);
					return;
				}
			}
		}

		foreach (FSMTransition t in _popupTransitions) {
			if (t.transition == eventName) {
				if (t.from == null || t.from == currentState) {
					Push(t.to);
					return;
				}

				if (t.to == currentState) {
					Pop();
					return;
				}
			}
		}

		Debug.LogWarning("Unit FSM: Event " + eventName + " not handled");
	}

	public void Log(string str) {
		if (!string.IsNullOrEmpty(log)) {
			log += "\n";
		}

		log += str;
	}

	public void Tick() {
		if (currentState != null) {
			currentState.Tick();
		}
	}

	public void RegisterState(FSMState state) {
		states.Add(state);
	}
	
	protected FSMState AddState(FSMState state) {
		states.Add(state);
		return state;
	}

	protected void AddGlobalTransition(int transition, FSMState to, FSMState[] exclude = null) {
		foreach (var s in states) {
			if (exclude == null || Array.IndexOf(exclude, s) == -1) {
				_transitions.Add(new FSMTransition(transition, s, to));
			}
		}
	}

	protected void AddTransition(int transition, FSMState stateFrom, FSMState to) {
		_transitions.Add(new FSMTransition(transition, stateFrom, to));
	}

	protected void AddGlobalPopupTransition(int transition, FSMState to, FSMState[] exclude = null) {
		foreach (var s in states) {
			if (exclude == null || Array.IndexOf(exclude, s) == -1) {
				_popupTransitions.Add(new FSMTransition(transition, s, to));
			}
		}
	}
	
	protected void AddPopupTransition(int transition, FSMState stateFrom, FSMState stateTo) {
		_popupTransitions.Add(new FSMTransition(transition, stateFrom, stateTo));
	}

	protected void Switch(FSMState to) {
		if (_isSwitching) {
			if (_nextState != null) {
				Debug.LogWarning("Too many state switches");
			}
			else {
				_nextState = to;
			}
		}
		else {
			_nextStateToPush = null;
			while (_queue.Count > 0) {
				Pop();
			}

			_isSwitching = true;
			if (currentState != null) {
				currentState.Exit();
			}

			currentState = to;
			currentState.Enter();
			_isSwitching = false;

			if (_nextState != null) {
				FSMState temp = _nextState;
				_nextState = null;
				Switch(temp);
			}
			else if (_nextStateToPush != null) {
				FSMState temp = _nextStateToPush;
				_nextStateToPush = null;
				Push(temp);
			}
		}
		
	}

	public void Push<T>() where T : FSMState {
		var state = State<T>();
		Push(state);
	}
	
	public T State<T>() where T : FSMState {
		foreach (FSMState state in states) {
			if (state is T) return (T) state;
		}

		Debug.LogWarningFormat("Can't find state {0}", typeof(T));
		return null;
	}


	public void Push(FSMState to) {
		if (_isSwitching) {
			if (_nextStateToPush != null) {
				Debug.LogWarning("Too many state switches");
			}
			else {
				_nextStateToPush = to;
			}
		}
		else {
			_isSwitching = true;
			if (currentState == to) {
				currentState.Exit();
				currentState.Enter();
			}
			else {
				if (currentState != null) {
					currentState.Pause();
				}

				_queue.Push(currentState);
				currentState = to;
				currentState.Enter();
			}

			_isSwitching = false;

			if (_nextState != null) {
				FSMState temp = _nextState;
				_nextState = null;
				Switch(temp);
			}
			else if (_nextStateToPush != null) {
				FSMState temp = _nextStateToPush;
				_nextStateToPush = null;
				Push(temp);
			}
		}
	}

	public void Pop() {
		if (_isSwitching) {
			Debug.LogWarning("Too many state switches");
		}
		else if (_queue.Count == 0) {
			Debug.LogWarning("Can't pop state. Current state is root state");
		}
		else {
			_isSwitching = true;
			if (currentState != null) {
				currentState.Exit();
			}

			currentState = _queue.Pop();
			currentState.Resume();
			_isSwitching = false;

			if (_nextState != null) {
				FSMState temp = _nextState;
				_nextState = null;
				Switch(temp);
			}
			else if (_nextStateToPush != null) {
					FSMState temp = _nextStateToPush;
					_nextStateToPush = null;
					Push(temp);
				}
		}
	}
}