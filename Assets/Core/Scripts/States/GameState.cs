﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameState : FSMState {
    public virtual bool IsInerruptable => false;

    public GameState(FSM fsm) : base(fsm) {
    }
}

public class GameState<UITYPE> : GameState
    where UITYPE : StateUI
{
    protected UITYPE ui;

    protected string _sceneName = null;

    public GameState(FSM fsm) : base(fsm)
    {
    }

    public sealed override void Enter()
    {
        base.Enter();

        if (!string.IsNullOrEmpty(_sceneName) && SceneManager.GetActiveScene().name != _sceneName)
        {
            SceneManager.sceneLoaded += SceneManagerOnSceneLoaded;
            SceneManager.LoadScene(_sceneName);
        }
        else
        {
            OnEntered();
        }
    }

    private void SceneManagerOnSceneLoaded(Scene arg0, LoadSceneMode loadSceneMode)
    {
        SceneManager.sceneLoaded -= SceneManagerOnSceneLoaded;
        OnEntered();
    }

    protected virtual void OnEntered()
    {
        ui = GameObject.FindObjectOfType<UITYPE>();
        if (ui == null)
        {
            Debug.LogWarning("Invalid UI. Current level : " + SceneManager.GetActiveScene().name + ", gui : "
                             + typeof(UITYPE).ToString());
        }
        else
        {
            ui.Show();
        }
    }

    public override void Exit()
    {
        base.Exit();
        if (ui != null)
        {
            ui.Hide();
            ui = null;
        }
    }

    public override void Pause()
    {
        base.Pause();
        if (ui != null)
        {
            ui.Pause();
        }
    }

    public override void Resume()
    {
        base.Resume();
        if (ui != null)
        {
            ui.Resume();
        }
    }
}