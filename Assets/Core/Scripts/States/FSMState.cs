﻿using System;

[Serializable]
public class FSMState
{
    public FSM fsm;

    public FSMState(FSM fsm)
    {
        this.fsm = fsm;
    }

    public virtual void Enter()
    {
    }

    public virtual void Exit()
    {
    }

    public virtual void Pause()
    {
    }

    public virtual void Resume()
    {
    }

    public virtual void Tick()
    {
    }

    public virtual void Update() {
    }
}