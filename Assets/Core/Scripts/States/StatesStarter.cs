﻿using UnityEngine;

public class StatesStarter : MonoBehaviour {
	public string startState;

	// Use this for initialization
	void Start() {
		Init();
	}

	private void Init() {
		States.Instance.StartFrom(startState);
	}

	// Update is called once per frame
	void Update() {
	}
}