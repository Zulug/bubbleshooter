﻿using System;
using UnityEngine;

public class GameStatesTicker : MonoBehaviour
{
    public Action OnFixedUpdate;
    public Action OnUpdate;
    public Action OnDisabled;
    
    private void FixedUpdate()
    {
        if (OnFixedUpdate != null) OnFixedUpdate();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F)) {
            Screen.fullScreen = !Screen.fullScreen;
        }
        if (OnUpdate != null) OnUpdate();
    }

    void OnDisable() {
        if (OnDisabled != null) OnDisabled();
    }
}
