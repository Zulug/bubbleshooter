﻿using UnityEngine;

public partial class States : FSM
{
    public enum Event
    {
        Warning
    }

    public enum Type
    {
        Initial
    }

    private static States _instance;

    private bool _isStarted = false;
    private GameStatesTicker _ticker;

    public static States Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new States();
            }

            return _instance;
        }
    }

    void OnDisable() {
        
    }

    public States()
    {
        _ticker = new GameObject().AddComponent<GameStatesTicker>();
        _ticker.name = "Ticker";
        Object.DontDestroyOnLoad(_ticker.gameObject);
        _ticker.OnFixedUpdate += FixedUpdate;
        _ticker.OnUpdate += Update;
        _ticker.OnDisabled += OnDisable;
    }

    private void FixedUpdate()
    {
        Tick();
    }

    private void Update()
    {
        if (currentState != null)
        {
            currentState.Update();
        }
    }

    public void StartFrom(string startState)
    {
        if (_isStarted) return;
        _isStarted = true;

        foreach (FSMState state in states)
        {
            if (state.GetType().Name == startState)
            {
                Switch(state);
                break;
            }
        }
    }

    public void StartFrom(Type startState)
    {
        if (_isStarted) return;
        _isStarted = true;

        switch (startState)
        {
            case Type.Initial:
            default:
                Switch(states[0]);
                break;
        }
    }

    public new static T State<T>() where T : FSMState
    {
        return ((FSM) Instance).State<T>();
    }

    public static GameState Current => Instance.currentState as GameState;

    public static void Change(FSMState to)
    {
        Instance.Switch(to);
    }

    public static void Change<T>() where T : FSMState
    {
        foreach (FSMState state in Instance.states)
        {
            if (state is T)
            {
                Change(state);
                return;
            }
        }

        Debug.LogWarningFormat("Can't find state {0}", typeof(T));
    }
}