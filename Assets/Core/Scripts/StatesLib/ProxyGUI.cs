using System;
using UnityEngine;

namespace Core.Scripts.StatesLib
{
    public class ProxyGUI<T> : StateUI where T : MonoBehaviour
    {
        public Transform content;
        public T windowPrefab;
        
        [NonSerialized][HideInInspector]
        public T window;

        public override void Show()
        {
            base.Show();
            window = Instantiate(windowPrefab, content, false);
        }

        public override void Hide()
        {
            base.Hide();
            Destroy(window.gameObject);
            window = null;
        }
    }
}