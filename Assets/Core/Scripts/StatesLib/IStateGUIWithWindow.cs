using UnityEngine;

public class IStateGUIWithWindow<T> : StateUI where T : MonoBehaviour
{
    public T window;

    public virtual void Awake() {
        window.gameObject.SetActive(false);
    }

    public override void Show()
    {
        base.Show();
        window.gameObject.SetActive(true);
    }

    public override void Hide()
    {
        base.Hide();
        window.gameObject.SetActive(false);
    }
}