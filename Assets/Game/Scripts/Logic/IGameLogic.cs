﻿using System.Collections.Generic;

public interface IGameLogic {
	GameEntity CreateGameListenerEntity();
	int CameraPosition { get; }
	List<int> GetGunAmmo { get; }
	Point? GetNearestEmptySlot(GameEntity bubble, int quarter);
	bool IsValidEmptyPosition(Point point);
	List<GameEntity> GetNeighbourEntities(Point point);
}