﻿using System;

public static class Settings {
	public const float TicksPerSecond = 8f;
	public const float SecondsPerTick = 1 / TicksPerSecond;

	public static ulong LevelToScore(int level) {
		return (ulong) Math.Pow(2, level);
	}
}