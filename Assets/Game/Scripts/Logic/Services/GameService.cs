﻿using System;
using System.Collections.Generic;
using System.Linq;
using Entitas;

public class GameService {
	private readonly List<Point> _evenNeighboursBuffer = new List<Point> {
		new Point(1, 0), new Point(1, -1), new Point(0, -1),
		new Point(-1, 0), new Point(0, 1), new Point(1, 1)
	};
	private readonly List<Point> _oddNeighboursBuffer = new List<Point> {
		new Point(1, 0), new Point(0, -1), new Point(-1, -1),
		new Point(-1, 0), new Point(-1, 1), new Point(0, 1)
	};
	private GameContext _context;
	private RandomService _random;
	private IGameOptions _options;

	private IGroup<GameEntity> _bubbles;
	private List<GameEntity> _bubblesBuffer;

	public void Init(Contexts contexts, RandomService random, IGameOptions options) {
		_context = contexts.game;
		_random = random;
		_options = options;
		_bubblesBuffer = new List<GameEntity>(_options.Width * _options.Height);
		_bubbles = _context.GetGroup(GameMatcher.Bubble);
	}

	public void Deinit() {
		_context = null;
	}

	public List<Point> GetNeighbourDirections(Point current) {
		if (current.y % 2 == 0) {
			return _evenNeighboursBuffer;
		} else {
			return _oddNeighboursBuffer;
		}
	}

	public int GenerateAmmo() {
		return _random.Int(_options.MinAmmoLevel, _options.MaxAmmoLevel + 1);
	}

	public Point GetFieldBounds() {
		if (_bubbles.count > 0) {
			var entity = _bubbles.GetEntities(_bubblesBuffer).First();
			Point result = new Point(entity.position.value.y, entity.position.value.y);
			foreach (var bubble in _bubbles) {
				var pos = bubble.position.value;
				if (pos.y > result.y) result.y = pos.y;
				else if (pos.y < result.x) result.x = pos.y;
			}

			return result;
		} else {
			return new Point(_context.camera.y, _context.camera.y);
		}
	}

	public bool AreFirstRowsFull(int rows) {
		for (int r = 0; r < rows; r++) {
			var y = _context.camera.y + r;
			for (int x = 0; x < _options.Width; x++) {
				var entity = _context.GetEntityWithPosition(new Point(x, y));
				if (entity == null) return false;
			}
		}

		return true;
	}

	public void TruncateField() {
		var fieldBounds = new Point(_context.camera.y - _options.Height + 1, _context.camera.y);
		foreach (GameEntity entity in _bubbles.GetEntities(_bubblesBuffer)) {
			if (entity.position.value.y < fieldBounds.x || entity.position.value.y > fieldBounds.y) {
				entity.isTruncatedBubble = true;
			}
		}
	}

	public void IncreaseScore(ulong increase) {
		var score = _context.score.value + increase;
		_context.ReplaceScore(score);
	}

	public void CheckLevel() {
		int level = CalculateLevelByScore(_context.score.value);
		if (level > _context.level.level) {
			_context.ReplaceLevel(level, CalculateScoreByLevel(level), CalculateScoreByLevel(level + 1));
		}
	}

	private int CalculateLevelByScore(ulong score) {
		int result;
		if (score < _options.FirstLevelScore) result = 1;
		else {
			var level = Math.Log((double) score / _options.FirstLevelScore, 2);
			if (level < 0) level = 0;
			result = (int) level + 2;
		}

		return result;
	}

	private ulong CalculateScoreByLevel(int level) {
		ulong result;
		if (level == 1) result = 0;
		else result = (ulong) (_options.FirstLevelScore * Math.Pow(2, level - 2));
		return result;
	}

	public bool AreNeighbours(Point a, Point b) {
		var neighbours = GetNeighbourDirections(a);
		foreach (Point p in neighbours) {
			if (a + p == b) return true;
		}

		return false;
	}

	public bool EntityIsDestroyed(GameEntity entity) {
		return entity.isFallingBubble || entity.isTruncatedBubble || entity.isExplodedBubble || entity.hasMergingBubble;
	}
}