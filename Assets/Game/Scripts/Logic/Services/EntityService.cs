﻿public class EntityService {
	private Contexts _contexts;
	private RandomService _random;
	private IGameOptions _options;

	public void Init(Contexts contexts, RandomService random, IGameOptions options) {
		_contexts = contexts;
		_random = random;
		_options = options;
	}

	public void Deinit() {
		_contexts = null;
	}

	public GameEntity CreateBubbleRelativeToCamera(int x, int y, int level) {
		var cameraPos = _contexts.game.camera.y;
		return CreateBubble(new Point(x, y + cameraPos), level, BubbleCreationType.New);
	}

	public GameEntity CreateBubble(Point pos, int level, BubbleCreationType type) {
		var entity = _contexts.game.CreateEntity();
		entity.AddBubble(level, type);
		entity.AddPosition(pos);
		return entity;
	}
}