public class RandomService {
	Randomizer _random;

	public void Initialize(int seed) {
		_random = new Randomizer(seed);
	}

	public float Float() {
		return _random.value;
	}

	public float Float(float minValue, float maxValue) {
		return minValue + (maxValue - minValue) * Float();
	}

	public int Int(int min, int max) {
		return _random.Range(min, max);
	}
}