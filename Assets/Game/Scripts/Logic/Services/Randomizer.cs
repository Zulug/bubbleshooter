using System;

[Serializable]
public class Randomizer {
	private Random _state;

	public Randomizer() {
		_state = new Random(0);
	}

	public Randomizer(int dataSeed) {
		_state = new Random(dataSeed);
	}

	public int Range(int min, int max) {
		return _state.Next(min, max);
	}

	public float Range(float min, float max) {
		return min + (max - min) * value;
	}

	public double Range(double min, double max) {
		return min + (max - min) * _state.NextDouble();
	}

	public float value => (float) _state.NextDouble();
}