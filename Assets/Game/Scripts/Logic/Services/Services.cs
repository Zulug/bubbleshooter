﻿using System;

public class Services {
	public EntityService entity = new EntityService();
	public RandomService gameRandom = new RandomService();
	public GameService game = new GameService();

	public void Init(Contexts contexts, IGameOptions options) {
		gameRandom.Initialize(options.Seed == -1 ? DateTime.UtcNow.Millisecond : options.Seed);
		entity.Init(contexts, gameRandom, options);
		game.Init(contexts, gameRandom, options);
	}

	public void Deinit() {
		game.Deinit();
		entity.Deinit();
	}
}