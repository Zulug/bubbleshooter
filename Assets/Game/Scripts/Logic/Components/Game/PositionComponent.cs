﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game]
public sealed class PositionComponent : IComponent {
	[PrimaryEntityIndex] public Point value;
}