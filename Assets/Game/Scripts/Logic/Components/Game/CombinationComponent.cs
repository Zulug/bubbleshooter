﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Event(EventTarget.Any)]
public sealed class CombinationComponent : IComponent {
	public Point[] positions;
	public Point targetPos;
}