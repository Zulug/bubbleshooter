﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Unique, Event(EventTarget.Any, priority: 1)]
public sealed class ScoreComponent : IComponent {
	public ulong value;
}