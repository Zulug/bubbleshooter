﻿using System.Collections.Generic;
using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Unique, Event(EventTarget.Any)]
public sealed class GunAmmoComponent : IComponent {
	public List<int> levels;
}