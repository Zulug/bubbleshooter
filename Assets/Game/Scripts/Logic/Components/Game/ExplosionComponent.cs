﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Event(EventTarget.Any)]
public sealed class ExplosionComponent : IComponent {
	public Point position;
	public int level;
}