﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Event(EventTarget.Self)]
public sealed class MergingBubbleComponent : IComponent {
	public Point target;
}