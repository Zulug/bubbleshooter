﻿using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Unique, Event(EventTarget.Any)]
public sealed class LevelComponent : IComponent {
	public int level;
	public ulong prevScore;
	public ulong nextScore;
}