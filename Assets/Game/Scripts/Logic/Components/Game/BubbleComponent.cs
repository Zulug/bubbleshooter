using Entitas;
using Entitas.CodeGeneration.Attributes;

[Game, Event(EventTarget.Any)]
public class BubbleComponent : IComponent {
	public int level;
	public BubbleCreationType type;
}

public enum BubbleCreationType {
	Merge,
	New,
	Player
}