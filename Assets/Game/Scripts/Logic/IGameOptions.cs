﻿public interface IGameOptions {
	int Seed { get; }
	int Width { get; }
	int Height { get; }
	int NonEmptyRows { get; }
	int RowsAtStart { get; }
	int RowsTillStop { get; }
	int ExplodeLevel { get; }
	int MinAmmoLevel { get; }
	int MaxAmmoLevel { get; }
	ulong FirstLevelScore { get; }
}