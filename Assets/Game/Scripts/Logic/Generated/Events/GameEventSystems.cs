//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.EventSystemsGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed class GameEventSystems : Feature {

    public GameEventSystems(Contexts contexts) {
        Add(new AnyBoardClearedEventSystem(contexts)); // priority: 0
        Add(new AnyBubbleEventSystem(contexts)); // priority: 0
        Add(new AnyCameraEventSystem(contexts)); // priority: 0
        Add(new AnyCombinationEventSystem(contexts)); // priority: 0
        Add(new ExplodedBubbleEventSystem(contexts)); // priority: 0
        Add(new AnyExplosionEventSystem(contexts)); // priority: 0
        Add(new FallingBubbleEventSystem(contexts)); // priority: 0
        Add(new AnyGunAmmoEventSystem(contexts)); // priority: 0
        Add(new AnyLevelEventSystem(contexts)); // priority: 0
        Add(new MergingBubbleEventSystem(contexts)); // priority: 0
        Add(new TruncatedBubbleEventSystem(contexts)); // priority: 0
        Add(new AnyTurnStartedEventSystem(contexts)); // priority: 0
        Add(new AnyTurnStartedRemovedEventSystem(contexts)); // priority: 0
        Add(new AnyScoreEventSystem(contexts)); // priority: 1
    }
}
