//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public AnyLevelListenerComponent anyLevelListener { get { return (AnyLevelListenerComponent)GetComponent(GameComponentsLookup.AnyLevelListener); } }
    public bool hasAnyLevelListener { get { return HasComponent(GameComponentsLookup.AnyLevelListener); } }

    public void AddAnyLevelListener(System.Collections.Generic.List<IAnyLevelListener> newValue) {
        var index = GameComponentsLookup.AnyLevelListener;
        var component = (AnyLevelListenerComponent)CreateComponent(index, typeof(AnyLevelListenerComponent));
        component.value = newValue;
        AddComponent(index, component);
    }

    public void ReplaceAnyLevelListener(System.Collections.Generic.List<IAnyLevelListener> newValue) {
        var index = GameComponentsLookup.AnyLevelListener;
        var component = (AnyLevelListenerComponent)CreateComponent(index, typeof(AnyLevelListenerComponent));
        component.value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemoveAnyLevelListener() {
        RemoveComponent(GameComponentsLookup.AnyLevelListener);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherAnyLevelListener;

    public static Entitas.IMatcher<GameEntity> AnyLevelListener {
        get {
            if (_matcherAnyLevelListener == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.AnyLevelListener);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherAnyLevelListener = matcher;
            }

            return _matcherAnyLevelListener;
        }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.EventEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public void AddAnyLevelListener(IAnyLevelListener value) {
        var listeners = hasAnyLevelListener
            ? anyLevelListener.value
            : new System.Collections.Generic.List<IAnyLevelListener>();
        listeners.Add(value);
        ReplaceAnyLevelListener(listeners);
    }

    public void RemoveAnyLevelListener(IAnyLevelListener value, bool removeComponentWhenEmpty = true) {
        var listeners = anyLevelListener.value;
        listeners.Remove(value);
        if (removeComponentWhenEmpty && listeners.Count == 0) {
            RemoveAnyLevelListener();
        } else {
            ReplaceAnyLevelListener(listeners);
        }
    }
}
