//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public MergingBubbleComponent mergingBubble { get { return (MergingBubbleComponent)GetComponent(GameComponentsLookup.MergingBubble); } }
    public bool hasMergingBubble { get { return HasComponent(GameComponentsLookup.MergingBubble); } }

    public void AddMergingBubble(Point newTarget) {
        var index = GameComponentsLookup.MergingBubble;
        var component = (MergingBubbleComponent)CreateComponent(index, typeof(MergingBubbleComponent));
        component.target = newTarget;
        AddComponent(index, component);
    }

    public void ReplaceMergingBubble(Point newTarget) {
        var index = GameComponentsLookup.MergingBubble;
        var component = (MergingBubbleComponent)CreateComponent(index, typeof(MergingBubbleComponent));
        component.target = newTarget;
        ReplaceComponent(index, component);
    }

    public void RemoveMergingBubble() {
        RemoveComponent(GameComponentsLookup.MergingBubble);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherMergingBubble;

    public static Entitas.IMatcher<GameEntity> MergingBubble {
        get {
            if (_matcherMergingBubble == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.MergingBubble);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherMergingBubble = matcher;
            }

            return _matcherMergingBubble;
        }
    }
}
