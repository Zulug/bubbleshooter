//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class InputEntity {

    public PlayerShotComponent playerShot { get { return (PlayerShotComponent)GetComponent(InputComponentsLookup.PlayerShot); } }
    public bool hasPlayerShot { get { return HasComponent(InputComponentsLookup.PlayerShot); } }

    public void AddPlayerShot(Point newPos) {
        var index = InputComponentsLookup.PlayerShot;
        var component = (PlayerShotComponent)CreateComponent(index, typeof(PlayerShotComponent));
        component.pos = newPos;
        AddComponent(index, component);
    }

    public void ReplacePlayerShot(Point newPos) {
        var index = InputComponentsLookup.PlayerShot;
        var component = (PlayerShotComponent)CreateComponent(index, typeof(PlayerShotComponent));
        component.pos = newPos;
        ReplaceComponent(index, component);
    }

    public void RemovePlayerShot() {
        RemoveComponent(InputComponentsLookup.PlayerShot);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class InputMatcher {

    static Entitas.IMatcher<InputEntity> _matcherPlayerShot;

    public static Entitas.IMatcher<InputEntity> PlayerShot {
        get {
            if (_matcherPlayerShot == null) {
                var matcher = (Entitas.Matcher<InputEntity>)Entitas.Matcher<InputEntity>.AllOf(InputComponentsLookup.PlayerShot);
                matcher.componentNames = InputComponentsLookup.componentNames;
                _matcherPlayerShot = matcher;
            }

            return _matcherPlayerShot;
        }
    }
}
