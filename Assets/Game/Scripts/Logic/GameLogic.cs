﻿using System.Collections.Generic;

public class GameLogic : IGameLogic {
	private Contexts _contexts;
	private Services _services;
	private Systems _systems;
	private uint _tick;
	private IGameOptions _options;

	public void Init(IGameOptions options) {
		_options = options;
		_contexts = new Contexts();
		_services = new Services();
		_services.Init(_contexts, _options);
		_systems = new Systems(_contexts, _services, _options);
	}

	public void Start() {
		_systems.Initialize();
	}

	public void Tick() {
		_tick++;
		_systems.Execute();
		_systems.Cleanup();
	}

	public void Deinit() {
		_systems.DeactivateReactiveSystems();
		_systems.TearDown();
		_systems = null;

		_services.Deinit();
		_services = null;

		_contexts = null;
		_options = null;
	}

	public void PlayerShot(Point pos) {
		var e = _contexts.input.CreateEntity();
		e.AddPlayerShot(pos);
	}

	public GameEntity CreateGameListenerEntity() {
		return _contexts.game.CreateEntity();
	}

	public int CameraPosition => _contexts.game.camera.y;

	public Point? GetNearestEmptySlot(GameEntity bubble, int quarter) {
		var pos = bubble.position.value;
		var neighbours = _services.game.GetNeighbourDirections(pos);
		var n = pos + neighbours[quarter];
		if (IsValidEmptyPosition(n)) return n;
		else {
			n = pos + neighbours[(quarter + 1) % 4];
			if (IsValidEmptyPosition(n)) return n;
			else {
				n = pos + neighbours[(quarter - 1) % 4];
				if (IsValidEmptyPosition(n)) return n;
				else return null;
			}
		}
	}

	public bool IsValidEmptyPosition(Point point) {
		bool isValid = point.x >= 0 && point.x < _options.Width
		                            && point.y <= _contexts.game.camera.y;
		if (!isValid) return false;

		bool isEmpty = _contexts.game.GetEntityWithPosition(point) == null;
		return isEmpty;
	}

	public List<GameEntity> GetNeighbourEntities(Point point) {
		var neighbours = _services.game.GetNeighbourDirections(point);
		var result = new List<GameEntity>();
		foreach (var p in neighbours) {
			var pos = point + p;
			var e = _contexts.game.GetEntityWithPosition(pos);
			if (e != null) {
				result.Add(e);
			}
		}

		return result;
	}

	public List<int> GetGunAmmo => _contexts.game.gunAmmo.levels;
}