﻿using System.Collections.Generic;
using Entitas;

public class CreateGameSystem : IInitializeSystem {
	private readonly GameContext _context;
	private readonly Services _service;
	private readonly IGameOptions _options;

	public CreateGameSystem(Contexts contexts, Services services, IGameOptions options) {
		_context = contexts.game;
		_service = services;
		_options = options;
	}

	public void Initialize() {
		_context.SetGunAmmo(new List<int> {_service.game.GenerateAmmo(), _service.game.GenerateAmmo()});
		_context.SetCamera(0);
		_context.SetScore(0);
		_context.SetLevel(1, 0, _options.FirstLevelScore);
		_context.isTurnStarted = false;
		_context.isStable = true;
	}
}