﻿using System.Collections.Generic;
using Entitas;

public class FillBoardIfEmptySystem : ReactiveSystem<GameEntity>, IInitializeSystem, ICleanupSystem {
	private Contexts _contexts;
	private Services _services;
	private IGameOptions _options;

	private IGroup<GameEntity> _bubbles;

	public FillBoardIfEmptySystem(Contexts contexts, Services services, IGameOptions options) : base(contexts.game) {
		_contexts = contexts;
		_services = services;
		_options = options;
		_bubbles = _contexts.game.GetGroup(GameMatcher.Bubble);
	}

	public void Initialize() {
		GenerateBoard();
	}

	private void GenerateBoard() {
		for (int x = 0; x < _options.Width; x++) {
			for (int y = 0; y < _options.RowsAtStart; y++) {
				_services.entity.CreateBubbleRelativeToCamera(x, -y, _services.game.GenerateAmmo());
			}
		}
	}

	protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) {
		return context.CreateCollector(GameMatcher.Bubble.Removed());
	}

	protected override bool Filter(GameEntity entity) {
		return !entity.hasBubble;
	}

	protected override void Execute(List<GameEntity> entities) {
		if (_bubbles.count == 0) {
			_contexts.game.isBoardCleared = true;
			GenerateBoard();
		}
	}

	public void Cleanup() {
		_contexts.game.isBoardCleared = false;
	}
}