﻿using Entitas;

public sealed class DestroyBubblesSystem : ICleanupSystem {
	readonly Contexts _contexts;
	private IGroup<GameEntity> _merges;

	public DestroyBubblesSystem(Contexts contexts) {
		_contexts = contexts;
		_merges = _contexts.game.GetGroup(GameMatcher.AnyOf(GameMatcher.MergingBubble, GameMatcher.FallingBubble, GameMatcher.TruncatedBubble,
			GameMatcher.ExplodedBubble));
	}

	public void Cleanup() {
		foreach (var entity in _merges.GetEntities()) {
			entity.Destroy();
		}
	}
}