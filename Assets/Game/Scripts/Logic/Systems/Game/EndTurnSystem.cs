﻿using System.Collections.Generic;
using Entitas;

public sealed class EndTurnSystem : ReactiveSystem<GameEntity> {
	private readonly Contexts _contexts;
	private readonly Services _services;

	public EndTurnSystem(Contexts contexts, Services services) : base(contexts.game) {
		_contexts = contexts;
		_services = services;
	}

	protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) {
		return context.CreateCollector(GameMatcher.AnyOf(GameMatcher.TurnStarted, GameMatcher.Stable));
	}

	protected override bool Filter(GameEntity entity) {
		return true;
	}

	protected override void Execute(List<GameEntity> entities) {
		if (_contexts.game.isTurnStarted && _contexts.game.isStable) {
			StartNextTurn();
		}

		_contexts.game.isStable = true;
	}

	private void StartNextTurn() {
		_services.game.CheckLevel();
		_contexts.game.isTurnStarted = false;
		var ammo = _contexts.game.gunAmmo.levels;
		ammo[0] = ammo[1];
		ammo[1] = _services.game.GenerateAmmo();
		_contexts.game.ReplaceGunAmmo(ammo);
	}
}