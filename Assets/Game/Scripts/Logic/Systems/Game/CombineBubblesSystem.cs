﻿using System;
using System.Collections.Generic;
using Entitas;

public sealed class CombineBubblesSystem : ReactiveSystem<GameEntity>, ICleanupSystem {
	private readonly Contexts _contexts;
	private Services _services;
	private readonly IGameOptions _options;
	private readonly IGroup<GameEntity> _entities;

	public CombineBubblesSystem(Contexts contexts, Services services, IGameOptions options) : base(contexts.game) {
		_contexts = contexts;
		_services = services;
		_options = options;
		_entities = _contexts.game.GetGroup(GameMatcher.Combination);
	}

	protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) {
		return context.CreateCollector(GameMatcher.Combination);
	}

	protected override bool Filter(GameEntity entity) {
		return entity.hasCombination;
	}

	protected override void Execute(List<GameEntity> entities) {
		foreach (GameEntity entity in entities) {
			var combination = entity.combination;
			GameEntity mergedBubble = _contexts.game.GetEntityWithPosition(combination.targetPos);

#if UNITY_EDITOR
			bool isValid = false;
			foreach (var p in combination.positions) {
				if (p == combination.targetPos) isValid = true;
			}

			if (!isValid) throw new Exception($"Invalid merging position: {combination.targetPos}");
			if (mergedBubble == null) throw new Exception($"Target merging bubble doesn't exist: {combination.targetPos}");
#endif

			var level = mergedBubble.bubble.level + combination.positions.Length - 1;
			foreach (Point position in combination.positions) {
				var bubble = _contexts.game.GetEntityWithPosition(position);

				if (position == combination.targetPos) {
					mergedBubble = bubble;
					mergedBubble.ReplaceBubble(level, BubbleCreationType.Merge);
					mergedBubble.isActiveBubble = true;
					_contexts.game.isStable = false;
				} else {
					bubble.AddMergingBubble(combination.targetPos);
				}
			}

			ulong increase = Settings.LevelToScore(mergedBubble.bubble.level);

			_services.game.IncreaseScore(increase);

			if (mergedBubble.bubble.level >= _options.ExplodeLevel) {
				var explosion = _contexts.game.CreateEntity();
				explosion.AddExplosion(mergedBubble.position.value, mergedBubble.bubble.level);
			}
		}
	}

	public void Cleanup() {
		foreach (var entity in _entities.GetEntities()) {
			entity.Destroy();
		}
	}
}