﻿using System.Collections.Generic;
using System.Linq;
using Entitas;

public sealed class FallSystem : ReactiveSystem<GameEntity> {
	readonly Contexts _contexts;
	private IGameOptions _options;


	private IGroup<GameEntity> _bubbles;
	private Services _services;

	public FallSystem(Contexts contexts, Services services, IGameOptions options) : base(contexts.game) {
		_contexts = contexts;
		_services = services;
		_options = options;
		_bubbles = _contexts.game.GetGroup(GameMatcher.Bubble);
	}

	protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) {
		return context.CreateCollector(GameMatcher.TurnStarted.Removed());
	}

	protected override bool Filter(GameEntity entity) {
		return !entity.isTurnStarted;
	}

	protected override void Execute(List<GameEntity> entities) {
		HashSet<Point> stuckEntities = new HashSet<Point>();
		HashSet<Point> queue = new HashSet<Point>();
		for (int i = 0; i < _options.Width; i++) {
			var pos = new Point(i, _contexts.game.camera.y);
			var entity = _contexts.game.GetEntityWithPosition(pos);
			if (entity != null && !_services.game.EntityIsDestroyed(entity)) {
				queue.Add(pos);
				stuckEntities.Add(pos);
			}
		}

		while (queue.Count > 0) {
			var current = queue.First();
			queue.Remove(current);
			List<Point> neighbours = _services.game.GetNeighbourDirections(current);
			foreach (Point direction in neighbours) {
				var pos = current + direction;
				if (!stuckEntities.Contains(pos) && !queue.Contains(pos)) {
					var e = _contexts.game.GetEntityWithPosition(pos);
					if (e != null && !_services.game.EntityIsDestroyed(e)) {
						stuckEntities.Add(pos);
						queue.Add(pos);
					}
				}
			}
		}

		foreach (var entity in _bubbles.GetEntities()) {
			if (!entity.isActiveBubble && !stuckEntities.Contains(entity.position.value)) {
				entity.isFallingBubble = true;
			}
		}
	}
}