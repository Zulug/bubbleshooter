﻿using System.Collections.Generic;
using Entitas;

public sealed class ExplosionSystem : ReactiveSystem<GameEntity>, ICleanupSystem {
	private readonly List<Point> _toDestroy = new List<Point>();

	private readonly Contexts _contexts;
	private readonly Services _services;
	private readonly IGroup<GameEntity> _entities;
	private readonly IGroup<GameEntity> _bubbles;

	public ExplosionSystem(Contexts contexts, Services services) : base(contexts.game) {
		_contexts = contexts;
		_services = services;
		_entities = contexts.game.GetGroup(GameMatcher.Explosion);
		_bubbles = contexts.game.GetGroup(GameMatcher.Bubble);
	}

	protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) {
		return context.CreateCollector(GameMatcher.Explosion);
	}

	protected override bool Filter(GameEntity entity) {
		return entity.hasExplosion;
	}

	protected override void Execute(List<GameEntity> entities) {
		foreach (GameEntity entity in entities) {
			var neighbours = _services.game.GetNeighbourDirections(entity.explosion.position);
			_toDestroy.Clear();
			var pos = entity.explosion.position;
			_toDestroy.Add(pos);

			foreach (Point point in neighbours) {
				_toDestroy.Add(pos + point);
			}

			foreach (Point point in _toDestroy) {
				var bubble = _contexts.game.GetEntityWithPosition(point);
				if (bubble != null) {
					bubble.isExplodedBubble = true;
				}
			}
		}
	}

	public void Cleanup() {
		foreach (var entity in _entities.GetEntities()) {
			entity.Destroy();
		}
	}
}