﻿using System.Collections.Generic;
using Entitas;

public sealed class MoveCameraSystem : ReactiveSystem<GameEntity> {
	private readonly Contexts _contexts;
	private readonly Services _services;
	private readonly IGameOptions _options;

	public MoveCameraSystem(Contexts contexts, Services services, IGameOptions options) : base(contexts.game) {
		_contexts = contexts;
		_services = services;
		_options = options;
	}

	protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) {
		return context.CreateCollector(GameMatcher.TurnStarted.Removed());
	}

	protected override bool Filter(GameEntity entity) {
		return !entity.isTurnStarted;
	}

	protected override void Execute(List<GameEntity> entities) {
		// if (the field's max height is greater than 8 than) move up
		// else if (the field's max height is less than 8 than)
		// 		if (the field's max height is less than 6 || the field has empty slots in first two rows) move down
		Point bounds = _services.game.GetFieldBounds();
		int height = bounds.y - bounds.x + 1;
		if (height > _options.Height) {
			MoveCameraDown();
		} else if (height < _options.Height) {
			if (height < _options.RowsTillStop || _services.game.AreFirstRowsFull(_options.NonEmptyRows)) {
				MoveCameraUp();
			}
		}
	}

	private void MoveCameraUp() {
		_contexts.game.ReplaceCamera(_contexts.game.camera.y + 1);
		_services.game.TruncateField();
		for (int x = 0; x < _options.Width; x++) {
			_services.entity.CreateBubbleRelativeToCamera(x, 0, _services.game.GenerateAmmo());
		}
	}

	private void MoveCameraDown() {
		_contexts.game.ReplaceCamera(_contexts.game.camera.y - 1);
		_services.game.TruncateField();
	}
}