﻿using System.Collections.Generic;
using System.Linq;
using Entitas;

public sealed class FindCombinationsSystem : ReactiveSystem<GameEntity> {
	private readonly HashSet<Point> _result = new HashSet<Point>();
	private readonly HashSet<Point> _traversed = new HashSet<Point>();
	private readonly HashSet<Point> _queue = new HashSet<Point>();
	private readonly HashSet<Point> _adjacent = new HashSet<Point>();

	private readonly Contexts _contexts;
	private readonly Services _services;

	public FindCombinationsSystem(Contexts contexts, Services services) : base(contexts.game) {
		_contexts = contexts;
		_services = services;
	}

	protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context) {
		return context.CreateCollector(GameMatcher.ActiveBubble);
	}

	protected override bool Filter(GameEntity entity) {
		return entity.isActiveBubble;
	}

	protected override void Execute(List<GameEntity> entities) {
		foreach (GameEntity entity in entities) {
			(var result, var targetPoint) = FindSimilarBubblesAround(entity);
			if (result.Count > 1) {
				var combination = _contexts.game.CreateEntity();
				combination.AddCombination(result.ToArray(), targetPoint);
				_contexts.game.isStable = false;
			}

			entity.isActiveBubble = false;
		}
	}

	private (HashSet<Point>, Point) FindSimilarBubblesAround(GameEntity sourceBubble) {
		var sourcePos = sourceBubble.position.value;
		var sourceLevel = sourceBubble.bubble.level;
		var targetPoint = sourcePos;

		_result.Clear();
		_traversed.Clear();
		_queue.Clear();
		_adjacent.Clear();
		_queue.Add(sourcePos);
		_adjacent.Add(sourcePos);

		// traversing neighbours to find all bubbles to combine
		while (_queue.Count != 0) {
			var current = _queue.First();
			_queue.Remove(current);
			_adjacent.Remove(current);
			_result.Add(current);
			_traversed.Add(current);
			List<Point> neighbours = _services.game.GetNeighbourDirections(current);
			foreach (Point direction in neighbours) {
				var pos = current + direction;
				if (!_traversed.Contains(pos) && !_queue.Contains(pos)) {
					var entity = _contexts.game.GetEntityWithPosition(pos);
					if (entity != null) {
						if (entity.bubble.level == sourceLevel) {
							_queue.Add(pos);
						}

						_adjacent.Add(pos);
					}
				}
			}
		}

		// finding the best place to merge
		foreach (Point point in _adjacent) {
			var entity = _contexts.game.GetEntityWithPosition(point);
			if (entity.bubble.level == sourceLevel + _result.Count - 1) {
				foreach (var p in _result) {
					if (_services.game.AreNeighbours(p, point)) {
						targetPoint = p;
						break;
					}
				}

				break;
			}
		}

		return (_result, targetPoint);
	}
}