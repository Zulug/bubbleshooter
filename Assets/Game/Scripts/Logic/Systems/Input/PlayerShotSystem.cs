﻿using System.Collections.Generic;
using Entitas;

public sealed class PlayerShotSystem : ReactiveSystem<InputEntity>, ICleanupSystem {
	readonly Contexts _contexts;
	private readonly Services _services;
	private IGroup<InputEntity> _inputs;

	public PlayerShotSystem(Contexts contexts, Services services) : base(contexts.input) {
		_contexts = contexts;
		_services = services;
		_inputs = _contexts.input.GetGroup(InputMatcher.PlayerShot);
	}

	protected override ICollector<InputEntity> GetTrigger(IContext<InputEntity> context) {
		return context.CreateCollector(InputMatcher.PlayerShot);
	}

	protected override bool Filter(InputEntity entity) {
		return entity.hasPlayerShot;
	}

	protected override void Execute(List<InputEntity> entities) {
		var inputEntity = entities.SingleEntity();
		var pos = inputEntity.playerShot.pos;

		var ammo = _contexts.game.gunAmmo.levels;
		var entity = _services.entity.CreateBubble(new Point(pos.x, pos.y), ammo[0], BubbleCreationType.Player);
		entity.isActiveBubble = true;

		_contexts.game.isTurnStarted = true;
	}

	public void Cleanup() {
		foreach (var entity in _inputs.GetEntities()) {
			entity.Destroy();
		}
	}
}