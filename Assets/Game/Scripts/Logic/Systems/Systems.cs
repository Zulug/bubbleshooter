﻿public class Systems : Feature {
	public Systems(Contexts contexts, Services services, IGameOptions options) {
		Add(new CreateGameSystem(contexts, services, options));
		Add(new FillBoardIfEmptySystem(contexts, services, options));

		Add(new PlayerInputSystems(contexts, services));

		Add(new FindCombinationsSystem(contexts, services));
		Add(new CombineBubblesSystem(contexts, services, options));
		Add(new FallSystem(contexts, services, options));
		Add(new ExplosionSystem(contexts, services));

		Add(new MoveCameraSystem(contexts, services, options));
		Add(new EndTurnSystem(contexts, services));

		Add(new GameEventSystems(contexts));

		Add(new DestroyBubblesSystem(contexts));
	}
}