﻿public class PlayerInputSystems : Feature {
	public PlayerInputSystems(Contexts contexts, Services services) {
		Add(new PlayerShotSystem(contexts, services));
	}
}