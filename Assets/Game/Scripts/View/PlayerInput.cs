﻿using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(BoxCollider2D))]
public class PlayerInput : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler, IAnyTurnStartedListener, IAnyTurnStartedRemovedListener {
	public Camera camera;
	ShootingState _state;
	private bool _inputIsLocked = false;
	private GameEntity _listener;


	public void Init(ShootingState state, GameEntity listener) {
		_state = state;
		_listener = listener;
		_listener.AddAnyTurnStartedListener(this);
		_listener.AddAnyTurnStartedRemovedListener(this);
	}

	public void Deinit() {
		_listener.RemoveAnyTurnStartedRemovedListener(this);
		_listener.RemoveAnyTurnStartedListener(this);
		_listener = null;
		_state = null;
	}

	public void OnPointerDown(PointerEventData eventData) {
		if (_inputIsLocked) return;
		var point = camera.ScreenToWorldPoint(eventData.position);
		_state.OnInputDown(point);
	}

	public void OnDrag(PointerEventData eventData) {
		if (_inputIsLocked) return;
		var point = camera.ScreenToWorldPoint(eventData.position);
		_state.OnInputDown(point);
	}

	public void OnPointerUp(PointerEventData eventData) {
		if (_inputIsLocked) return;
		var point = camera.ScreenToWorldPoint(eventData.position);
		_state.OnInputUp(point);
	}

	public void LockInput() {
		_inputIsLocked = true;
	}

	public void OnAnyTurnStarted(GameEntity entity) {
		_inputIsLocked = entity.isTurnStarted;
	}

	public void OnAnyTurnStartedRemoved(GameEntity entity) {
		_inputIsLocked = entity.isTurnStarted;
	}
}