﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class GunView : MonoBehaviour, IAnyGunAmmoListener {
	public GunAmmo bubblePrefab;
	public float shotSpeed = 10;

	[SerializeField] private float secondAmmoScale = 0.75f;

	private GunAmmo currentAmmo;
	private GunAmmo secondAmmo;
	private IGameLogic _logic;
	private BubbleField _field;
	private GameOptions _options;
	private GameEntity _listener;

	public void Init(IGameLogic logic, BubbleField field, GameOptions options, GameEntity listener) {
		_logic = logic;
		_field = field;
		_options = options;
		_listener = listener;

		_listener.AddAnyGunAmmoListener(this);
	}

	public void Deinit() {
		_listener.RemoveAnyGunAmmoListener(this);
		_listener = null;
		_field = null;
		_options = null;
	}

	public void OnAnyGunAmmo(GameEntity entity, List<int> levels) {
		var ammo = _logic.GetGunAmmo;

		if (secondAmmo == null) {
			currentAmmo = Instantiate(bubblePrefab, transform.position, Quaternion.identity, transform);
			currentAmmo.Init(_field, _options, ammo[0]);
		} else {
			secondAmmo.transform.DOMove(transform.position, Settings.SecondsPerTick);
			secondAmmo.AnimateScaleToFullSize();
			currentAmmo = secondAmmo;
		}

		var secondAmmoPos = transform.position - new Vector3(_field.BubbleSize, 0, 0);
		secondAmmo = Instantiate(bubblePrefab, secondAmmoPos, Quaternion.identity, transform);
		secondAmmo.Init(_field, _options, ammo[1]);
		secondAmmo.transform.localScale *= secondAmmoScale;
	}

	public void Shoot(List<Vector3> losPoints, Point targetPos, Action callback) {
		currentAmmo.ActivateTrail();
		var sequence = DOTween.Sequence();
		var finalPoint = (Vector3) _field.PointToPosition(targetPos);
		finalPoint.z = transform.position.z;
		var prevPoint = losPoints[0];
		for (int i = 1; i < losPoints.Count - 1; i++) {
			var prev = losPoints[i - 1];
			var point = losPoints[i];
			if (point.x > transform.position.x) point.x -= _field.BubbleSize / 2;
			else point.x += _field.BubbleSize / 2;
			var dist = Vector3.Distance(prev, point);
			sequence.Append(currentAmmo.transform.DOMove(point, dist / shotSpeed).SetEase(Ease.Linear));
			prevPoint = point;
		}

		var d = Vector3.Distance(prevPoint, finalPoint);
		sequence.Append(currentAmmo.transform.DOMove(finalPoint, d / shotSpeed).SetEase(Ease.Linear));
		sequence.AppendCallback(() => {
			currentAmmo.Deinit();
			GameObject.Destroy(currentAmmo.gameObject);
			currentAmmo = null;

			callback();
		});
		sequence.Play();
	}
}