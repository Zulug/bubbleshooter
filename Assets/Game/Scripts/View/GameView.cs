using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Serialization;

public class GameView : MonoBehaviour, IAnyBubbleListener, IAnyCameraListener, IAnyCombinationListener, IAnyBoardClearedListener, IAnyExplosionListener {
	public BubbleView bubblePrefab;

	public BubbleSelectionView selection;

	public Transform bubblesContainer;
	public SpriteRenderer background;
	[FormerlySerializedAs("inputBox")] public PlayerInput input;
	public GunView gunView;

	[SerializeField] private GameObject rightWall;
	[SerializeField] private GameObject leftWall;
	[SerializeField] private GameObject topWall;
	[SerializeField] private LineRenderer line;

	private IGameLogic _logic;
	private GameOptions _options;
	private GameEntity _listener;
	private BubbleField _field;

	List<BubbleView> _bubbles = new List<BubbleView>();

	public void Init(IGameLogic logic, GameEntity listener, GameOptions options) {
		_logic = logic;
		_options = options;
		_listener = listener;
		_listener.AddAnyBubbleListener(this);
		_listener.AddAnyCameraListener(this);
		_listener.AddAnyCombinationListener(this);
		_listener.AddAnyBoardClearedListener(this);
		_listener.AddAnyExplosionListener(this);

		_field = new BubbleField();
		_field.Init(_logic, _options, background);

		gunView.Init(_logic, _field, _options, _listener);

		input.Init(States.State<ShootingState>(), _listener);
	}

	public void Deinit() {
		gunView.Deinit();
		_field.Deinit();

		_listener.RemoveAnyExplosionListener(this);
		_listener.RemoveAnyBoardClearedListener(this);
		_listener.RemoveAnyCombinationListener(this);
		_listener.RemoveAnyCameraListener(this);
		_listener.RemoveAnyBubbleListener(this);
		_listener = null;
		_options = null;
		_logic = null;

		input.Deinit();
	}

	public void OnAnyBubble(GameEntity entity, int level, BubbleCreationType type) {
		var bubble = FindBubble(entity);
		if (bubble != null) {
			bubble.UpdateView();
		} else {
			bubble = Instantiate(bubblePrefab, bubblesContainer);
			Vector2 coords = _field.PointToPosition(entity.position.value);
			bubble.transform.position = new Vector3(coords.x, coords.y, bubblesContainer.transform.position.z - 1);
			bubble.Init(entity, _field, _options, type);
			bubble.OnDestroyed += OnBubbleDestroyed;
			_bubbles.Add(bubble);

			var neighbours = _logic.GetNeighbourEntities(bubble.Entity.position.value);
			foreach (var e in neighbours) {
				var neighbourBubble = FindBubble(e);
				if (neighbourBubble != null) {
					var pos = neighbourBubble.transform.position;
					var dir = pos - bubble.transform.position;
					var sequence = DOTween.Sequence();
					sequence.Append(neighbourBubble.transform.DOMove(pos + dir * 0.2f, Settings.SecondsPerTick / 2));
					sequence.Append(neighbourBubble.transform.DOMove(pos, Settings.SecondsPerTick / 2));
					sequence.Play();
				}
			}
		}
	}

	private BubbleView FindBubble(GameEntity entity) {
		return _bubbles.Find(b => b.Entity == entity);
	}

	private void OnBubbleDestroyed(BubbleView bubble) {
		bubble.OnDestroyed -= OnBubbleDestroyed;
		_bubbles.Remove(bubble);
	}

	public bool IsSideWall(GameObject obj) {
		return obj == rightWall || obj == leftWall;
	}

	public bool IsTopWall(GameObject obj) {
		return obj == topWall;
	}

	public Point? GetAimPos(Vector3 point, Collider2D collider) {
		Point? result;
		if (IsTopWall(collider.gameObject)) {
			result = _field.TopPositionToPoint(point.x);
		} else {
			var bubble = _bubbles.Find(b => b.gameObject == collider.gameObject);
			result = _field.GetNearestEmptySlot(bubble, point);
		}

		return result;
	}

	public void DrawAim(List<Vector3> points, int level, Point point) {
		DrawSelection(point, level);

		var array = points.ToArray();
		for (int i = 0; i < array.Length; i++) {
			array[i] -= gameObject.transform.position;
		}

		line.enabled = true;
		line.positionCount = array.Length;
		line.SetPositions(array);
	}

	private void ClearSelection() {
		selection.Deinit();
	}

	private void DrawSelection(Point point, int level) {
		Vector2 coords = _field.PointToPosition(point);
		selection.transform.position = new Vector3(coords.x, coords.y, bubblesContainer.transform.position.z - 1);
		selection.Init(_field, _options, level);
	}

	public void ClearAim() {
		line.enabled = false;
		ClearSelection();
	}

	public void OnAnyCamera(GameEntity entity, int y) {
		foreach (BubbleView bubble in _bubbles) {
			Vector2 coords = _field.PointToPosition(bubble.Entity.position.value);
			bubble.transform.DOMove(new Vector3(coords.x, coords.y, bubblesContainer.transform.position.z - 1), Settings.SecondsPerTick);
		}
	}

	public void OnAnyCombination(GameEntity entity, Point[] positions, Point targetPos) {
		// var combination = entity.combination;
		// foreach (var point in combination.positions) {
		//     var bubble = _bubbles.Find(b => b.Entity.position.value == point);
		//     bubble.MergeTo(combination.targetPos);
		// }
	}

	public void OnAnyBoardCleared(GameEntity entity) {
		Instantiate(_options.perfectEffect, transform.position + Vector3.back * 2, Quaternion.identity, transform);
	}

	public void OnAnyExplosion(GameEntity entity, Point position, int level) {
		BoomEffect effect = Instantiate(_options.boomEffect, transform);
		effect.transform.position = (Vector3) _field.PointToPosition(position) + Vector3.back * 3;
		foreach (var sprite in effect.sprites) {
			sprite.color = _options.GetColor(level);
		}

		var cam = Camera.main;
		cam.DOShakePosition(Settings.SecondsPerTick, Vector3.one);
		effect.transform.DOScale(3, Settings.SecondsPerTick)
			.OnComplete(() => Destroy(effect.gameObject));
	}
}