using UnityEngine;

public class BubbleField {
	private IGameLogic _logic;
	private Bounds _bounds;
	private GameOptions _options;
	private float _bubbleSize;
	private Vector2 _zeroPos; // for even row
	private float _rowHeight;

	public float BubbleSize => _bubbleSize;

	public void Init(IGameLogic logic, GameOptions options, SpriteRenderer sprite) {
		_logic = logic;
		_options = options;
		_bounds = sprite.bounds;
		var backWidthInBubbles = _options.Width - 0.5f;
		// var _bubbleSizeX = _bounds.size.x / backWidthInBubbles;
		// var _bubbleSizeY = _bounds.size.y * Mathf.Sqrt(3) / 2 / (_options.height + 1);
		// _bubbleSize = Math.Min(_bubbleSizeX, _bubbleSizeY);
		_bubbleSize = _bounds.size.x / backWidthInBubbles;
		_zeroPos = new Vector2(_bounds.min.x + _bubbleSize / 2,
			_bounds.max.y + _bubbleSize / 2);
		_rowHeight = _bubbleSize * Mathf.Sqrt(3) / 2;
	}

	public Vector2 PointToPosition(Point point) {
		var even = point.y % 2 == 0;
		point.y -= _logic.CameraPosition;
		return PointToPositionCameraRelative(point, even);
	}

	private Vector2 PointToPositionCameraRelative(Point point, bool even) {
		float offset = even ? 0f : -0.5f;
		return new Vector2(_zeroPos.x + _bubbleSize * (point.x + offset), _zeroPos.y + _rowHeight * point.y);
	}

	public Point? TopPositionToPoint(float posX) {
		var y = _logic.CameraPosition;
		var even = y % 2 == 0;
		float offset = even ? 0f : 0.5f;
		var result = new Point(Mathf.RoundToInt((posX - _zeroPos.x) / _bubbleSize + offset), y);
		if (_logic.IsValidEmptyPosition(result)) return result;
		else return null;
	}

	public void Deinit() {
		_logic = null;
	}

	public Point? GetNearestEmptySlot(BubbleView bubble, Vector3 hitPos) {
		Vector3 bubblePos = bubble.transform.position;
		int quarter = 0; // 3 | 0
		if (hitPos.x > bubblePos.x) quarter = hitPos.y > bubblePos.y ? 0 : 1; // -----
		else quarter = hitPos.y > bubblePos.y ? 3 : 2; // 2 | 1
		return _logic.GetNearestEmptySlot(bubble.Entity, quarter);
	}
}