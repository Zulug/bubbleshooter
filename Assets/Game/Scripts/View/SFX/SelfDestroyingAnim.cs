﻿using UnityEngine;

public class SelfDestroyingAnim : MonoBehaviour {
	public void OnFinish() {
		Destroy(gameObject);
	}
}