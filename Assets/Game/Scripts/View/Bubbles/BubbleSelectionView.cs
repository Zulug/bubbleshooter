﻿public class BubbleSelectionView : BubbleBaseView {
	public override void Init(BubbleField field, GameOptions options, int level) {
		gameObject.SetActive(true);
		base.Init(field, options, level);
		var color = renderer.color;
		color.a = 0.5f;
		renderer.color = color;
	}

	public override void Deinit() {
		gameObject.SetActive(false);
		base.Deinit();
	}
}