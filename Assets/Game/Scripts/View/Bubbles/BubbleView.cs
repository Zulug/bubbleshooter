using System;
using DG.Tweening;
using Entitas;
using UnityEngine;
using Random = UnityEngine.Random;

public class BubbleView : BubbleViewWithText, IMergingBubbleListener, IFallingBubbleListener, ITruncatedBubbleListener, IExplodedBubbleListener {
	public GameEntity Entity { get; private set; }

	public Action<BubbleView> OnDestroyed;

	public void Init(GameEntity entity, BubbleField field, GameOptions options, BubbleCreationType type) {
		var level = entity.bubble.level;
		base.Init(field, options, level);
		Entity = entity;
		entity.AddMergingBubbleListener(this);
		entity.AddFallingBubbleListener(this);
		entity.AddTruncatedBubbleListener(this);
		entity.AddExplodedBubbleListener(this);
		Entity.OnDestroyEntity += OnEntityDestroyed;

		switch (type) {
			case BubbleCreationType.Player:
			case BubbleCreationType.Merge:
				break;
			case BubbleCreationType.New:
				var colorFrom = renderer.color;
				colorFrom.a = 0;
				renderer.color = colorFrom;
				colorFrom.a = 1;
				renderer.DOColor(colorFrom, Settings.SecondsPerTick * 2);
				break;
			default:
				throw new ArgumentOutOfRangeException(nameof(type), type, null);
		}
	}

	public void UpdateView() {
		var level = Entity.bubble.level;
		UpdateView(level);
	}

	private void OnEntityDestroyed(IEntity entity) {
		Deinit();
		OnDestroyed?.Invoke(this);
	}

	public override void Deinit() {
		base.Deinit();
		Entity.OnDestroyEntity -= OnEntityDestroyed;
		Entity = null;
	}

	public void OnMergingBubble(GameEntity entity, Point target) {
		ParticleSystem mergeEffect = Instantiate(_options.mergeEffect, transform.position, Quaternion.identity);
		var main = mergeEffect.main;
		main.startColor = renderer.color;

		var position = _field.PointToPosition(target);
		transform.DOMove(position, Settings.SecondsPerTick).OnComplete(() => Destroy(gameObject));
	}

	public void OnFallingBubble(GameEntity entity) {
		gameObject.layer = LayerMask.NameToLayer("Default");
		var rigidbody = gameObject.AddComponent<Rigidbody2D>();
		var force = Random.insideUnitCircle;
		if (force.y > 0) force.y *= -1;
		rigidbody.AddForce(force * 4, ForceMode2D.Impulse);
		Destroy(gameObject, 2f);
	}

	public void OnTruncatedBubble(GameEntity entity) {
		var colorFrom = renderer.color;
		colorFrom.a = 0;
		renderer.DOColor(colorFrom, Settings.SecondsPerTick).OnComplete(() => Destroy(gameObject));
	}

	public void OnExplodedBubble(GameEntity entity) {
		ParticleSystem mergeEffect = Instantiate(_options.mergeEffect, transform.position, Quaternion.identity);
		var main = mergeEffect.main;
		main.startColor = renderer.color;

		OnFallingBubble(entity);
	}
}