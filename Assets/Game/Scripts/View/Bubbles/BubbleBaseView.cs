﻿using DG.Tweening;
using UnityEngine;

public abstract class BubbleBaseView : MonoBehaviour {
	public new SpriteRenderer renderer;
	private Bounds _initialBounds;
	protected BubbleField _field;
	protected GameOptions _options;

	private void Awake() {
		_initialBounds = renderer.bounds;
	}

	public virtual void Init(BubbleField field, GameOptions options, int level) {
		_field = field;
		_options = options;
		renderer.color = _options.GetColor(level);

		float originalSize = _initialBounds.size.x;
		float targetSize = _field.BubbleSize;
		transform.localScale = targetSize / originalSize * Vector3.one;
	}

	protected virtual void UpdateView(int level) {
		renderer.color = _options.GetColor(level);
	}

	public virtual void Deinit() {
		_options = null;
		_field = null;
	}

	public void AnimateScaleToFullSize() {
		float originalSize = _initialBounds.size.x;
		float targetSize = _field.BubbleSize;
		transform.DOScale(targetSize / originalSize * Vector3.one, Settings.SecondsPerTick);
	}
}