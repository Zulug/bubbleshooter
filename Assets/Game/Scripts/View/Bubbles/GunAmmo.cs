﻿using UnityEngine;

public class GunAmmo : BubbleViewWithText {
	public TrailRenderer trail;

	public override void Init(BubbleField field, GameOptions options, int level) {
		base.Init(field, options, level);
	}

	public void ActivateTrail() {
		trail.gameObject.SetActive(true);
		var keys = trail.colorGradient.colorKeys;
		keys[0].color = renderer.color;
		trail.colorGradient.SetKeys(keys, trail.colorGradient.alphaKeys);
		trail.widthMultiplier = transform.localScale.x;
	}

	public override void Deinit() {
		base.Deinit();
	}
}