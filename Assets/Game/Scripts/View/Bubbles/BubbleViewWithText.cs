﻿using TMPro;

public class BubbleViewWithText : BubbleBaseView {
	public TMP_Text text;

	public override void Init(BubbleField field, GameOptions options, int level) {
		base.Init(field, options, level);
		text.text = FormatScore(Settings.LevelToScore(level));
	}

	protected override void UpdateView(int level) {
		base.UpdateView(level);
		text.text = FormatScore(Settings.LevelToScore(level));
	}

	private string FormatScore(ulong score) {
		if (score < 1024) {
			return score.ToString();
		} else if (score < 1048576) {
			score /= 1024;
			return score.ToString() + "K";
		} else {
			score /= 1048576;
			return score.ToString() + "M";
		}
	}
}