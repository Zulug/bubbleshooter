public class MainMenuUI : IStateGUIWithWindow<MainMenuWindow> {
	public override void Awake() {
		base.Awake();
		States.Instance.RegisterState(new MainMenuState(States.Instance));
	}

	public override void Show() {
		base.Show();
		window.Show();
	}

	public override void Hide() {
		base.Hide();
		window.Hide();
	}
}