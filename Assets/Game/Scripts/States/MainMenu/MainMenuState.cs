using UnityEngine;

public class MainMenuState : GameState<MainMenuUI> {
	public MainMenuState(FSM fsm) : base(fsm) {
	}

	protected override void OnEntered() {
		base.OnEntered();
		Debug.Log("MainMenuState OnEnter");
	}

	public override void Exit() {
		Debug.Log("MainMenuState OnExit");
		base.Exit();
	}
}