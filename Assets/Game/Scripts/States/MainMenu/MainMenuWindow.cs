using UnityEngine;

public class MainMenuWindow : MonoBehaviour {
	private MainMenuState _state;

	public void Show() {
		_state = States.State<MainMenuState>();
	}

	public void Hide() {
		_state = null;
	}

	public void OnPlayClick() {
		States.Change<ShootingState>();
	}
}