using DG.Tweening;
using UnityEngine;

public class LevelUpWindow : MonoBehaviour {
	private LevelUpState _state;
	private Vector3 _originalPos;
	[SerializeField] private Transform foreground;

	void Awake() {
		_originalPos = foreground.position;
	}

	public void Show() {
		_state = States.State<LevelUpState>();
		foreground.position = new Vector3(_originalPos.x, 30);
		foreground.DOMove(_originalPos, 0.3f);
	}

	public void Hide() {
		_state = null;
	}

	public void OnCloseClick() {
		foreground.DOMove(new Vector3(_originalPos.x, -30), 0.3f)
			.OnComplete(() => { States.Instance.Pop(); });
	}
}