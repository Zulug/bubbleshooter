public class LevelUpUI : IStateGUIWithWindow<LevelUpWindow> {
	public override void Awake() {
		base.Awake();
		States.Instance.RegisterState(new LevelUpState(States.Instance));
	}

	public override void Show() {
		base.Show();
		window.Show();
	}

	public override void Hide() {
		base.Hide();
		window.Hide();
	}
}