using UnityEngine;

public class LevelUpState : GameState<LevelUpUI> {
	public LevelUpState(FSM fsm) : base(fsm) {
	}

	protected override void OnEntered() {
		base.OnEntered();
		Debug.Log("LevelUpState OnEnter");
	}

	public override void Exit() {
		Debug.Log("LevelUpState OnExit");
		base.Exit();
	}
}