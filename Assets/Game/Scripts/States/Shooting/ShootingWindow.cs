using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ShootingWindow : MonoBehaviour, IAnyScoreListener, IAnyLevelListener {
	public TMP_Text scoreText;
	public Slider slider;
	public TMP_Text prevLevelText;
	public TMP_Text nextLevelText;
	public Image prevLevelImage;
	public Image nextLevelImage;
	public Image sliderForeground;

	private ShootingState _state;
	private GameEntity _listener;
	private GameOptions _options;
	private int _prevLevel = 0;
	private ulong _score;
	private ulong _prevScore;
	private ulong _nextScore;

	public void Show() {
		_state = States.State<ShootingState>();
	}

	public void Hide() {
		_state = null;
	}

	public void Init(GameEntity listener, GameOptions options) {
		_prevLevel = 0;
		_options = options;
		_listener = listener;
		_listener.AddAnyScoreListener(this);
		_listener.AddAnyLevelListener(this);
	}

	public void Deinit() {
		_listener.RemoveAnyScoreListener(this);
		_listener.RemoveAnyLevelListener(this);
		_listener = null;
		_options = null;
	}

	public void OnAnyScore(GameEntity entity, ulong value) {
		_score = value;
		UpdateScore();
	}

	private void UpdateScore() {
		scoreText.text = _score.ToString();
		var diff = _nextScore - _prevScore;
		var internalScore = _score - _prevScore;
		slider.value = (float) internalScore / diff;
	}

	public void OnAnyLevel(GameEntity entity, int level, ulong prevScore, ulong nextScore) {
		_prevScore = prevScore;
		_nextScore = nextScore;
		sliderForeground.color = _options.GetColor(level);
		prevLevelImage.color = _options.GetColor(level);
		nextLevelImage.color = _options.GetColor(level + 1);
		prevLevelText.text = level.ToString();
		nextLevelText.text = (level + 1).ToString();

		UpdateScore();

		if (level > _prevLevel && level > 1) {
			_prevLevel = level;
			States.Instance.Push<LevelUpState>();
		}
	}
}