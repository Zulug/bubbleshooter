public class ShootingUI : IStateGUIWithWindow<ShootingWindow> {
	public GameView view;

	public override void Awake() {
		base.Awake();
		States.Instance.RegisterState(new ShootingState(States.Instance));
	}

	public override void Show() {
		base.Show();
		window.Show();
		view.gameObject.SetActive(true);
	}

	public override void Hide() {
		base.Hide();
		view.gameObject.SetActive(false);
		window.Hide();
	}
}