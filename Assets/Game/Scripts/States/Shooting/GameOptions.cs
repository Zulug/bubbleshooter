using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class GameOptions : ScriptableObject, IGameOptions {
#if UNITY_EDITOR
	[MenuItem("Assets/Data/Create GameOptions")]
	static void CreateConfig() {
		GameOptions config = CreateInstance<GameOptions>();

		string path = AssetDatabase.GenerateUniqueAssetPath("Assets/Game/Resources/GameOptions.asset");
		AssetDatabase.CreateAsset(config, path);
		AssetDatabase.SaveAssets();
		AssetDatabase.Refresh();
		EditorUtility.FocusProjectWindow();
		Selection.activeObject = config;
	}
#endif

	[SerializeField] private int _seed = -1;
	public int Seed => _seed;

	[SerializeField] private int _width = 6;
	public int Width => _width;
	[SerializeField] private int _height = 8;
	public int Height => _height;

	[SerializeField] private int _nonEmptyRows = 2;
	public int NonEmptyRows => _nonEmptyRows;
	[SerializeField] private int _rowsAtStart = 4;
	public int RowsAtStart => _rowsAtStart;
	[SerializeField] private int _rowsTillStop = 6;
	public int RowsTillStop => _rowsTillStop;

	[SerializeField] private int _explodeLevel = 11;
	public int ExplodeLevel => _explodeLevel;

	[SerializeField] private int _minAmmoLevel = 1;
	public int MinAmmoLevel => _minAmmoLevel;
	[SerializeField] private int _maxAmmoLevel = 6;
	public int MaxAmmoLevel => _maxAmmoLevel;

	[SerializeField] private ulong _firstLevelScore = 512;
	public ulong FirstLevelScore => _firstLevelScore;

	[SerializeField] public List<Color> bubbleColors;

	public ParticleSystem mergeEffect;
	public GameObject perfectEffect;
	public BoomEffect boomEffect;

	public Color GetColor(int level) {
		return bubbleColors[level % bubbleColors.Count];
	}
}