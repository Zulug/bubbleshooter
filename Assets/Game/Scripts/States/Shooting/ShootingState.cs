using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;

public class ShootingState : GameState<ShootingUI> {
	private GameLogic _logic;
	private RaycastHit2D[] _raycastBuffer = new RaycastHit2D[1];
	private LayerMask fieldBoundsLayer = LayerMask.NameToLayer("FieldBounds");
	private List<Vector3> _losPoints = new List<Vector3>();

	private Point? _aimPos = null;
	private float _leftTime = 0;
	private GameEntity _listener;

	public ShootingState(FSM fsm) : base(fsm) {
	}

	protected override void OnEntered() {
		base.OnEntered();
		Debug.Log("ShootingState OnEnter");
		DOTween.SetTweensCapacity(500, 125);

		_logic = new GameLogic();
		var options = Resources.Load<GameOptions>("GameOptions");

		_logic.Init(options);
		_listener = _logic.CreateGameListenerEntity();
		ui.view.Init(_logic, _listener, options);
		ui.window.Init(_listener, options);

		_logic.Start();
	}

	public override void Tick() {
		base.Tick();
		int ticks = 0;

		_leftTime += Time.fixedDeltaTime;
		float tickTime = Settings.SecondsPerTick;
		while (_leftTime > tickTime && ticks < 10) {
			_leftTime -= tickTime;
			ticks++;
			_logic.Tick();
		}
	}

	private void ForceTick() {
		_leftTime = 0;
		_logic.Tick();
	}

	public override void Exit() {
		Debug.Log("ShootingState OnExit");
		ui.window.Deinit();
		ui.view.Deinit();
		_listener.Destroy();
		_listener = null;
		_logic.Deinit();
		_logic = null;

		_aimPos = null;

		base.Exit();
	}

	public void OnInputDown(Vector3 point) {
		AimAt(point);
	}

	private void AimAt(Vector3 point) {
		Collider2D aimedCollider = CheckAim(point);

		if (aimedCollider != null) {
			_aimPos = ui.view.GetAimPos(_losPoints.Last(), aimedCollider);
			if (_aimPos != null) {
				ui.view.DrawAim(_losPoints, _logic.GetGunAmmo.First(), (Point) _aimPos);
			} else {
				ui.view.ClearAim();
			}
		} else {
			ui.view.ClearAim();
			_aimPos = null;
		}
	}

	private Collider2D CheckAim(Vector3 point) {
		Collider2D result = null;
		var gunPos = ui.view.gunView.transform.position;
		Vector2 dir = point - gunPos;
		_losPoints.Clear();
		_losPoints.Add(gunPos);

		var results = Physics2D.RaycastNonAlloc(gunPos, dir, _raycastBuffer, float.PositiveInfinity, 1 << fieldBoundsLayer);
		if (results > 0) {
			var hit = _raycastBuffer[0];
			_losPoints.Add(new Vector3(hit.point.x, hit.point.y, gunPos.z));
			if (ui.view.IsSideWall(hit.collider.gameObject)) {
				dir.x *= -1;
				results = Physics2D.RaycastNonAlloc(hit.point + dir * 0.001f, dir, _raycastBuffer, float.PositiveInfinity, 1 << fieldBoundsLayer);
				if (results > 0) {
					hit = _raycastBuffer[0];
					if (!ui.view.IsSideWall(hit.collider.gameObject)) {
						_losPoints.Add(new Vector3(hit.point.x, hit.point.y, gunPos.z));
						;
						result = hit.collider;
					}
				}
			} else {
				result = hit.collider;
			}
		}

		return result;
	}

	public void OnInputUp(Vector3 point) {
		AimAt(point);
		if (_aimPos != null) {
			ui.view.input.LockInput();
			ui.view.ClearAim();
			ui.view.gunView.Shoot(_losPoints, (Point) _aimPos, () => {
				_logic.PlayerShot((Point) _aimPos);
				ForceTick();
			});
		}
	}
}